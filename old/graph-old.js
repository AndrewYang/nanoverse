function log2 (x) {
    return Math.log(x) * Math.LOG2E;
}
function vector(x, y) {
    return { x: x, y: y };
}
function copyVector(vector) {
    return { x: vector.x, y: vector.y };
}
function modulus(vector) {
    return Math.sqrt(vector.x * vector.x + vector.y * vector.y);
}
function normalise(vector) {
    var r = modulus(vector);
    return { x: vector.x / r, y: vector.y / r};
}
function orthogonalVector(vector, magnitude) {
    var r = modulus(vector);
    return { x: vector.y / r * magnitude, y: -vector.x / r * magnitude };
}
function addVector(v1, v2) {
    return { x: v1.x + v2.x, y: v1.y + v2.y };
}
function subtractVector(v1, v2) {
    return { x: v1.x - v2.x, y: v1.y - v2.y };
}
function multiply(vector, delta) {
    return { x: vector.x * delta, y: vector.y * delta };
}
function clamp(x, min, max) {
    if (x > max) return max;
    if (x < min) return min;
    return x;
}
function middle(v1, v2) {
    return { x: 0.5 * (v1.x + v2.x), y: 0.5 * (v1.y + v2.y) }
}
function componentToHex(c) {
    var hex = parseInt(c.toFixed(0)).toString(16);
    return hex.length == 1 ? "0" + hex : hex;
}
function rgbToHex(c) {
    return "#" + componentToHex(c.r) + componentToHex(c.g) + componentToHex(c.b);
}
function initMatrix(rows, cols) {
    var matrix = [];
    for (var r = 0; r < rows; r++) {
        var row = [];
        for (var c = 0; c < cols; c++) {
            row.push(0);
        }
        matrix.push(row);
    }
    return matrix;
}
function printMatrix(matrix) {
    var str = '';
    for (var i = 0; i < matrix.length; i++) {
        for (var x = 0; x < matrix[i].length; x++)
            str += matrix[i][x] + '\t';
        str += '\n'
    }
    console.log(str);
}
function L2Vect(v1, v2) {
    return L2(v1.x, v1.y, v2.x, v2.y);
}
function L2(x1, y1, x2, y2) {
    var d1 = x1 - x2;
    var d2 = y1 - y2;

    var d = d1 * d1 + d2 * d2;
    if (d <= 0) return 0;
    return Math.sqrt(d);
}
function mergeArrays(a1, a2) {
    var temp = {};
    var i;
    for (i = 0; i < a1.length; i++) {
        temp[a1[i]] = true;
    }
    for (i = 0; i < a2.length; i++) {
        temp[a2[i]] = true;
    }
    return Object.keys(temp);
}
function easeInOut(x) {
    // Input x: [0, 1]
    // Output y: [0, 1] representing location along quadratic ease-in-out function

    if (x <= 0.5) return 2 * x * x;
    return 4 * x - 2 * x * x - 1;
}
function easeOut(x) {
    return Math.sqrt(x);
}

class Dag {

    /* the structure of the json object is:

        accounts: array[string],
        txs: array[] {
            f: from_index (int),
            t: to_index (int)
            a: [amount, nano],
            h: [hash (string)],
            d: [timestamp (long), default = 0]
        } 
    */
    constructor(dag) {

        this.accounts = {};             // accounts will be keyed using account ids
        this.txs = {};                  // txs are mapped using their hash
        
        if (dag == undefined) {
            console.log('dag is undefined');
            return;
        }
        this.focalPoint = dag.focalPoint;   // This is where we draw the map icon - could be a tx or account
        this.isComplete = dag.complete;
        this.rootAccount = dag.focalPoint.r; // the center - this is what everything is drawn wrt

        var accs = dag.accounts;
        var parents = dag.parents;
        if (accs != undefined && parents != undefined) {
            for (var i = 0; i < accs.length; i++) {
                // this object is used to store graphics, etc.
                this.accounts[accs[i]] = {
                    pos: { 
                        set: false,
                        opening_state: 0,
                        block_location: vector(0, 0), 
                        location: vector(0, 0),
                        parent: accs[parents[i]]
                    },
                    deg: 0
                };
            }
        }
        
        var txs = dag.txs;
        if (txs != undefined) {
            for (var i = 0; i < txs.length; i++) {

                // Add in the actual addresses
                var t = txs[i];
                if (t.s != undefined && t.f != undefined && t.t != undefined && t.h != undefined && t.h.length > 0 && !this.txs[t.h]) {
                    this.txs[t.h] = {
                        opening_state: 0,
                        source: accs[t.s],
                        from: accs[t.f],
                        to: accs[t.t],
                        a: t.a,
                        d: t.d,
                        h: t.h
                    };
                    this.accounts[accs[t.f]].deg++;
                    this.accounts[accs[t.t]].deg++;
                }
            }
        }
    }

    add(dag) {

        var accs = dag.accounts;
        var parents = dag.parents;
        if (accs != undefined && parents != undefined) {
            for (var i = 0; i < accs.length; i++) {
                if (!this.accounts[accs[i]]) {
                    this.accounts[accs[i]] = { 
                        pos: { 
                            set: false,
                            opening_state: 0,
                            block_location: vector(0, 0), 
                            location: vector(0, 0),
                            parent: accs[parents[i]]
                        }, 
                        deg: 0 
                    }
                }
            }
        }

        var txs = dag.txs;
        if (txs != undefined) {
            for (var i = 0; i < txs.length; i++) {
                // Add in the actual addresses
                var t = txs[i];
                if (t.s != undefined && t.f != undefined && t.t != undefined && t.h != undefined && t.h.length > 0 && !this.txs[t.h]) {
                    this.txs[t.h] = {
                        opening_state: 0,
                        source: accs[t.s],
                        from: accs[t.f],
                        to: accs[t.t],
                        a: t.a,
                        d: t.d,
                        h: t.h
                    };
                }
                this.accounts[accs[t.f]].deg++;
                this.accounts[accs[t.t]].deg++;
            }
        }
    }

    setLocation (address, block_location, rel_location) {
        var pos = this.accounts[address].pos;
        pos.set = true;
        pos.block_location = block_location;
        pos.location = rel_location;
    }
    getAccounts () {
        var accs = Object.keys(this.accounts);
        var result = [];
        for (var i = 0; i < accs.length; i++) {
            result.push({
                account: accs[i],
                deg: this.accounts[accs[i]].deg
            });
        }
        return result;
    }
    getSecondaryAccounts () {
        // Gets all accounts other than root account
        var accs = Object.keys(this.accounts);
        var index = accs.indexOf(this.rootAccount);
        if (index >= 0) {
            accs.splice(index, 1);
        }
        return accs;
    }
    getAccountPositions (frame) {
        
        var accs = Object.keys(this.accounts);
        var result = [];
        for (var i = 0; i < accs.length; i++) {
            var account = this.accounts[accs[i]];

            var p = frame.transformVector(account.pos.location);
            result.push({
                account: accs[i],
                display: 0 <= p.x && p.x <= frame.width && 0 <= p.y && p.y <= frame.height,
                x: p.x + 'px',
                y: p.y + 'px'
            });
        }
        return result;
    }
    getTransactionPositions (frame) {
        var txs = Object.keys(this.txs);
        var result = [];
        for (var i = 0; i < txs.length; i++) {
            var tx = this.txs[txs[i]];

            if (tx.a > frame.minTxSizeForLabel) {
                var p = frame.transformVector(tx.graphics.label_location);

                if (0 <= p.x && p.x <= frame.width && 0 <= p.y && p.y <= frame.height) {
                    result.push({ h: tx.h, a: tx.a, x: p.x + 'px', y: p.y + 'px'});
                }
            }
        }
        return result;
    }

    // This method is used to conserve memory - all objects that fall outside the frame's context is removed.
    // This can be computationally intensive so try not to call this method too frequently
    clearObjectsOutsideContext (frame) {

        // Whether each account is in the context is stored as a boolean permanently. 
        // Watch out for whether this has any performance drags.
        var accounts = Object.keys(this.accounts);
        for (var i = 0; i < accounts.length; i++) {
            var pos = this.accounts[accounts[i]].pos;
            pos.isInContext = frame.isInContext(pos.location);
            //console.log('is in context?\t' + accounts[i] + '\t' + pos.isInContext);
        }

        // O(1) loop through the transactions 
        var transactions = Object.keys(this.txs);
        for (var i = 0; i < transactions.length; i++) {
            var tx = this.txs[transactions[i]];

            var fromAccount = this.accounts[tx.from].pos,
                toAccount = this.accounts[tx.to].pos;

            if (fromAccount.isInContext || toAccount.isInContext) {

                // If any one of them is in the context, then both are in the context
                if (!fromAccount.isInContext) fromAccount.isInContext = true;
                if (!toAccount.isInContext) toAccount.isInContext = true;
            }
        }

        // Another O(1) loop once all the accounts have been correctly set - removes all txn outside of context
        for (var i = 0; i < transactions.length; i++) {
            var tx = this.txs[transactions[i]];
            if (!this.accounts[tx.from].pos.isInContext || !this.accounts[tx.to].pos.isInContext) {
                // unset the tx field
                //console.log('removing ' + transactions[i]);
                //console.log('\t' + tx.from + '\t' + tx.to + '\t' + this.accounts[tx.from].pos.isInContext + '\t' + this.accounts[tx.to].pos.isInContext + '\t');
                delete this.txs[transactions[i]];
            }
        }

        // Removes all accounts outside of context - frees up memory for accounts
        for (var i = 0; i < accounts.length; i++) {
            var a = accounts[i];
            if (!this.accounts[a].pos.isInContext) {
                //console.log('\tremoving\t' + a + '\t');
                delete this.accounts[a];
            }
        }
    }
}
class DagGraphics {

    constructor(xBlocks, yBlocks, blur) {
        this.xBlocks = xBlocks;
        this.yBlocks = yBlocks;
        this.blur = blur;
        this.alpha = 0.1;                       // Scalar representing how heavily we weigh distance penalty vs density penalty
        this.reset();
    }

    reset () {
        this.blocksRelativeToImageFrame = true;                             // blocks are either relative to the image frame or the default reference frame
        this.totalDensityMap = initMatrix(this.yBlocks, this.xBlocks);      // the sum of densities of the nodes already on the map
        this.totalDensityWeight = 0;                                        // weighted sum of nodes already on the map - we use this to normalise the totalDensity
    }

    // This method re-bases the graphics blocks and re-calculates the density map based on the default frame 
    // Note that this does not correct for frame.scale, which means we could have high density sections next 
    // to low density ones. 
    resetBlocks (dag, frame) {
        this.reset();
        this.blocksRelativeToImageFrame = false;

        var blockWidth = frame.defaultFrame.width / this.xBlocks,
            blockHeight = frame.defaultFrame.height / this.yBlocks;
        
        // rebase the blocks + recalculate density map
        var accounts = Object.keys(dag.accounts);
        for (var i = 0; i < accounts.length; i++) {
            var acc = dag.accounts[accounts[i]];
            var pos = frame.transformVector(acc.pos.location);
            acc.pos.block_location.x = Math.floor((pos.x - frame.defaultFrame.left) / blockWidth);
            acc.pos.block_location.y = Math.floor((pos.y - frame.defaultFrame.top) / blockHeight);
            this.addNode(acc.pos.block_location, calculateNodeWeight(acc.deg));
        }
    }

    findLowestPenaltyBlock (sourceNodeBlock) {

        // find the block [i,x] which minimises the objective function 
        var lowestPenaltyBlock = vector(0, 0);
        var lowestPenalty = Number.MAX_VALUE;

        //console.log('total penalty\t' + this.totalDensityWeight);
        //var s = '', s1 = '', s2 = '';
        for (var y = 0; y < this.yBlocks; y++) {
            for (var x = 0; x < this.xBlocks; x++) {
                var densityPenalty = this.totalDensityMap[y][x] / this.totalDensityWeight;
                var distancePenalty = this.calculateDistancePenalty(sourceNodeBlock, x, y);
                var loss = this.calculatePenalty(densityPenalty, distancePenalty);
                if (loss < lowestPenalty) {
                    lowestPenalty = loss;
                    lowestPenaltyBlock.x = x;
                    lowestPenaltyBlock.y = y;
                }
                //s += loss + '\t'; s1 += densityPenalty + '\t'; s2 += distancePenalty + '\t';
            }
            //s += '\n'; s1 += '\n'; s2 += '\n';
        }
        //console.log('combined');
        //console.log(s);
        //console.log('density');
        //console.log(s1);
        //console.log('distancePenalty');
        //console.log(s2);
        //console.log(sourceNodeBlock.x + '\t' + sourceNodeBlock.y + '\t' + lowestPenaltyBlock.x + '\t' + lowestPenaltyBlock.y)
        return lowestPenaltyBlock;
    }
    calculateDistancePenalty (sourceVector, x, y) {

        var dx = sourceVector.x - x;
        var dy = sourceVector.y - y;
        return Math.sqrt((dy * dy + dx * dx) / (2 * (this.xBlocks - 1) * (this.yBlocks - 1)));
    }
    calculatePenalty (densityPenalty, distancePenalty) {
        return densityPenalty + distancePenalty * this.alpha;
    }

    // modify the total density penalty when a new block is added 
    addNode (block, weight) {
        for (var y = 0; y < this.yBlocks; y++) {
            for (var x = 0; x < this.xBlocks; x++) {
                // Add gaussian to density map
                this.totalDensityMap[y][x] += weight * Math.exp(-L2(x, y, block.x, block.y));
            }
        }
        this.totalDensityWeight += weight;
    }
    sampleNodeLocationWithinBlock (block, frame) {
        // Call this method once we have already sampled the block
        // We will now sample the exact location within the block
        // Note that everything is represented as proportion of the screen width
        var blockHeight = 1 / this.yBlocks, blockWidth = 1 / this.xBlocks;
        var p = vector(
            blockWidth * (block.x + Math.random()), 
            blockHeight * (block.y + Math.random()) * frame.height / frame.width // change of basis to proportion of width
        );

        if (this.blocksRelativeToImageFrame) {
            return p;
        }

        // Otherwise - blocks are relative to the default reference frame
        var imageFrameWidth = frame.scale * frame.width;
        return {
            x: (p.x * frame.defaultFrame.width + frame.defaultFrame.left + frame.translate.x) / imageFrameWidth,
            y: (p.y * frame.defaultFrame.width + frame.defaultFrame.top + frame.translate.y) / imageFrameWidth
        };
    }
}
class ReferenceFrame {

    constructor (canvasWidth, canvasHeight) {
        this.width = canvasWidth;
        this.height = canvasHeight;

        this.scale = 1;
        this.defaultScale = 2.5;
        this.defaultFrame = {
            top: (1 - this.defaultScale) / 2 * this.height,
            left: (1 - this.defaultScale) / 2 * this.width,
            right: (1 + this.defaultScale) / 2 * this.width,
            bottom: (1 + this.defaultScale) / 2 * this.height,
            width: this.defaultScale * this.width,
            height: this.defaultScale * this.height
        };

        this.translate = vector(0, 0);

        this.minTxSizeForLabel = 1;
    }

    // Everything is represented as a proportion of width in the proportion vector
    transformVector (proportionVector) {
        return { 
            x: proportionVector.x * this.width * this.scale - this.translate.x, 
            y: proportionVector.y * this.width * this.scale - this.translate.y
        };
    }

    // Returns the center of the screen in px coordinates
    center () {
        return {
            x: this.width / 2,
            y: this.height / 2
        }
    }

    // Returns whether the position (x,y) is in the frame under the current transformation
    isInFrame (position) {
        var W = this.width * this.scale;
        var x = position.x * W - this.translate.x;
        var y = position.y * W - this.translate.y;
        return 0 <= x && x <= this.width && 
                0 <= y && y <= this.height;
    }

    // Returns whether a position (x,y) remains in the context. This is more strict than not 
    // being in the frame - once an object leaves the context it is removed from memory.
    isInContext (position) {
        var W = this.width * this.scale;
        var x = position.x * W - this.translate.x;
        var y = position.y * W - this.translate.y;
        return -this.width <= x && x <= 2 * this.width && 
               -this.height <= y && y <= 2 * this.height;
    }

}