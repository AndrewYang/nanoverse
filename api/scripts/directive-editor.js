app.directive('nnEditor', function($http, $timeout) {
    return {
        restrict: 'E',
        templateUrl: "/api/editor.html",
        scope: {
            ui: '=',
            console: '=',
            editable: '='
        },
        link: function(scope, element, attrs) {

            var defaultLines = 20;
            scope.lineNumbers = getLineNumbers(defaultLines);
            scope.gui = {};

            var watch = scope.editable ? 'console.in.json' : 'console.out.json';
            scope.$watch(watch, function (newVal, oldVal) {
                if (newVal) {
                    var n = newVal.trim().split('\n').length;
                    scope.lineNumbers = getLineNumbers(Math.max(defaultLines, n));
                }
            });

            scope.postJson = function () {

                if (!scope.editable) return;

                var json = scope.console.in.json;
                if (!json) return;

                scope.console.out.json = '';
                scope.console.out.statusCode = undefined;
                scope.console.out.loading = true;

                $http.post('/api/node', json).then(
                    function success (res) {
                        console.log(res);
                        scope.console.out.json = formatJson(res.data);
                        scope.console.out.statusCode = res.status;
                        scope.console.out.loading = false;
                    },
                    function error (err) {
                        console.log(err);
                        scope.console.out.json = formatJson(err.data);
                        scope.console.out.statusCode = err.status;
                        scope.console.out.loading = false;
                    }
                )
            }

            scope.copyToClipboard = function () {
                var json = scope.editable ? scope.console.in.json : scope.console.out.json;
                copyToClipboard(json);
            }

            scope.clear = function () {
                if (scope.editable) {
                    scope.console.in.json = '';
                } else {
                    scope.console.out.json = '';
                    scope.console.out.statusCode = undefined;
                }
                scope.lineNumbers = getLineNumbers(defaultLines);
            }
        }
    }
});

function getLineNumbers(n) {
    if (n <= 0) return '';

    var s = ''
    for (var i = 1; i <= n; i++) {
        s += i + '\n';
    }
    return s;
}
function initLines(n) {
    var array = [];
    for (var i = 0; i <= n; i++) {
        array.push('');
    }
    return array;
}
function copyToClipboard (text) {
    if (!navigator.clipboard) {
        return false;
    }
    navigator.clipboard.writeText(text);
    return true;
}