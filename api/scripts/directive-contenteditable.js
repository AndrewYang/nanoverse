app.directive('contenteditable', function () {
    return {
        restrict: 'A',
        require: '?ngModel',
        link: function (scope, element, attrs, ngModel) {

            element.on('keydown', function (e) {
                
                // tab key
                if (e.keyCode === 9) { 

                    e.preventDefault();  // this will prevent us from tabbing out of the editor
            
                    // now insert four non-breaking spaces for the tab key
                    var editor = element[0];
                    var doc = editor.ownerDocument.defaultView;
                    var sel = doc.getSelection();
                    var range = sel.getRangeAt(0);
            
                    var tabNode = document.createTextNode("\u00a0\u00a0");
                    range.insertNode(tabNode);
            
                    range.setStartAfter(tabNode);
                    range.setEndAfter(tabNode); 
                    sel.removeAllRanges();
                    sel.addRange(range);
                }
            });
            element.on('blur keyup change', function (evt) {
                scope.$apply(read);
            });

            function read() {
                ngModel.$setViewValue(element.html());
            }
            ngModel.$render = function() {
                element.html(ngModel.$viewValue || "");
            };
        }
    };
});