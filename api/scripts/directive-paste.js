app.directive('nnPasteable', function($http, $timeout) {
    return {
        restrict: 'E',
        templateUrl: "/api/pasteable.html",
        scope: {
            text: '=',
        },
        link: function(scope, element, attrs) {

            scope.pasted = false;

            scope.copy = function () {
                copyToClipboard(scope.text);
            }
        }
    }
});