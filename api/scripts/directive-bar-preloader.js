app.directive('nnBarPreloader', function($interval) {
    return {
        restrict: 'E',
        template: "<div class=\"bar-preloader\"><div ng-repeat=\"bar in bars\" ng-style=\"{width: bar.width, height: bar.height, left: bar.left, top: bar.top, 'background-color': color}\"></div></div>",
        scope: {
            color: '=',
        },
        link: function(scope, element, attrs) {

            scope.bars = [
                { top: 0, left: 0, height: '100%', width: '16%' },
                { top: 0, left: '42%', height: '100%', width: '16%' },
                { top: 0, left: '84%', height: '100%', width: '16%' }
            ];

            var period = 10;
            var previous = -1, next = 0;
            var timer = $interval(function () {
                
                previous = (previous + 1) % period;
                next = (next + 1) % period;

                if (previous < scope.bars.length) {
                    // reset previous bar
                    var b = scope.bars[previous];
                    b.top = '10%';
                    b.left = 42 * previous + '%';
                    b.height = '80%';
                    b.width = '16%';
                }

                if (next < scope.bars.length) {
                    // enlarge bar
                    var b = scope.bars[next];
                    b.top = 0;
                    b.left = (42 * next - 2) + '%';
                    b.height = '100%';
                    b.width = '20%';
                }
            }, 100);

            scope.$on('$destroy', function() {
                if (timer != undefined) {
                    $interval.cancel(timer);
                    timer = undefined;
                }
            });
        }
    }
});