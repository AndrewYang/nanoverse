var debug = false;
var nodeRadius = 10;

app.controller('graphCtrl', function ($scope, $http, $document, $routeParams) {

    $scope.nodes = [];
    $scope.isAnimating = false;  // we avoid doing intensive ui when this is true
    $scope.currFocalPoint = undefined;  // will draw a map symbol, as well as colour
    $scope.loading = true;
    $scope.cursor = "grab";

    var canvas = document.getElementById('chart-graph');
    var ctx = canvas.getContext('2d');
        ctx.canvas.width = window.innerWidth;
        ctx.canvas.height = window.innerHeight;

    // TODO: choose xDim, yDim to match screen
    var xDim = 51, yDim = 25, blur = 5 / (xDim + yDim);
    var dag = undefined;
    var dagGraphics = new DagGraphics(xDim, yDim, blur);
    var frame;

    $scope.$on('dagInit', function (event, value) {

        if (value == undefined) return;
        dag = value;
        $scope.ui.loading = false;

        dagGraphics.reset();
        frame = new ReferenceFrame(ctx.canvas.width, ctx.canvas.height);
        $scope.currFocalPoint = dag.focalPoint;

        calculateAccountPositions(dag, dagGraphics, frame);
        calculateTransactionPositions(dag);
        shiftToFocus(dag, frame);

        $scope.isAnimating = true;

        // Initiate opening animation
        window.requestAnimationFrame(function (timestamp) {
            entryAnimationFrame({
                ctx: ctx,
                dag: dag,
                frame: frame,
                zoom: true,
                originalScale: frame.scale,
                originalTranslate: copyVector(frame.translate),
                startTime: timestamp,
                timestamp: timestamp,
                isLastSequence: false,
                focalPoint: $scope.currFocalPoint,
                darkTheme: $scope.ui.darkTheme,
                callback: function (dag) {
                    // HTML bindings
                    $scope.nodes = dag.getAccountPositions(frame);
                    $scope.txs = dag.getTransactionPositions(frame);
                    
                    if (!dag.isComplete) {
                        var secondaryAccounts = dag.getSecondaryAccounts();
                        if (secondaryAccounts.length > 0) {

                            // if there is something else 
                            $scope.io.searchSecondaryAddresses(secondaryAccounts);
                            $scope.$apply();
                            return;
                        }
                    }
                    // If the dag is complete
                    $scope.isAnimating = false;
                    $scope.$apply();
                }
            });
        });
    }, 
    true);

    $scope.$on('dagUpdate', function (event, value) {

        if (value == undefined) return;
        dag = value;
        $scope.ui.loading = false;

        calculateAccountPositions(dag, dagGraphics, frame);
        calculateTransactionPositions(dag);

        // Initiate opening animation
        window.requestAnimationFrame(function (timestamp) {
            entryAnimationFrame({
                ctx: ctx,
                dag: dag,
                frame: frame,
                zoom: false,
                startTime: timestamp,
                timestamp: timestamp,
                isLastSequence: true,
                focalPoint: $scope.currFocalPoint,
                darkTheme: $scope.ui.darkTheme,
                callback: function (dag) {
                    // HTML bindings
                    $scope.nodes = dag.getAccountPositions(frame);
                    $scope.txs = dag.getTransactionPositions(frame);
                    $scope.isAnimating = false;
                    $scope.$apply();
                }
            });
        });
    },
    true);

    $scope.$watch('ui.darkTheme', function (newVal, oldVal) {
        if (newVal != undefined && newVal != oldVal) {
            paint(ctx, dag, frame, $scope.isAnimating, $scope.currFocalPoint, $scope.ui.darkTheme);
        }
    });

    var timer = undefined;
    $scope.getAccountDetails = function (node) {

        // Mouseover is for loading bars
        node.mouseover = true;

        // doing to to avoid too many queries
        if (timer != undefined) {
            clearTimeout(timer);
            timer = undefined;
        }

        timer = setTimeout(function (node) {
            $http.post('/account', {
                address: node.account
            }).then(
                function success(res) {
                    console.log(res);

                    node.balance = res.data.balance;
                    node.balanceInt = getIntegerPart(res.data.balance);
                    node.balanceDecimal = getDecimalPart(res.data.balance);
                    
                    node.pendingBalance = res.data.pending;
                    node.pendingBalanceInt = getIntegerPart(res.data.pending);
                    node.pendingBalanceDecimal = getDecimalPart(res.data.pending);

                    node.txs = res.data.transactions;
                    node.pendingTxs = res.data.pt;

                    // Holds the unique colours
                    var palette = {};
                    if (node.txs) {
                        for (var i = 0; i < node.txs.length; i++) {
                            var tx = node.txs[i];
                            tx.date = $scope.formatDate(tx.t * 1000);
                            tx.a = $scope.io.lengthenAddress(tx.a);
                            tx.address = formatShortenedDisplayAddress(tx.a);
                            palette[tx.a] = rgbToHex({r: 170 + Math.random() * 55, g: 170 + Math.random() * 55, b: 170 + Math.random() * 55});
                        }
                    }
                    if (node.pendingTxs) {
                        for (var i = 0; i < node.pendingTxs.length; i++) {
                            var tx = node.pendingTxs[i];
                            tx.date = $scope.formatDate(tx.t * 1000);
                            tx.a = $scope.io.lengthenAddress(tx.a);
                            tx.address = formatShortenedDisplayAddress(tx.a);
                            palette[tx.a] = rgbToHex({r: 170 + Math.random() * 55, g: 170 + Math.random() * 55, b: 170 + Math.random() * 55});
                        }
                    }

                    // Only apply the palette at the very end
                    if (node.txs) {
                        for (var i = 0; i < node.txs.length; i++) {
                            node.txs[i].color = palette[node.txs[i].a];
                        }
                    }
                    if (node.pendingTxs) {
                        for (var i = 0; i < node.pendingTxs.length; i++) {
                            node.pendingTxs[i].color = palette[node.pendingTxs[i].a];
                        }
                    }

                    node.loaded = true;
                    node.hasError = false;
                },
                function error(err) {
                    node.loaded = true;
                    node.hasError = true;
                }
            )
        }, 400, node);
    }

    function formatShortenedDisplayAddress(address) {
        if (!address || address.length < 10) return undefined;
        return address.substring(0, 10) + "..." + address.substring(address.length - 5);
    }
    $scope.cancelGetAccountDetails = function (node) {

        // Mouseover is for loading bars
        node.mouseover = false;

        if (timer != undefined) {
            clearTimeout(timer);
            timer = undefined;
        }
    }

    // dual on-mousedown/on-mouseup event - Go to node. For some reason ng-click doesn't work
    var currGoToAccount;
    $scope.beginGoToVector = function (vector) {
        currGoToAccount = vector.account;
    }
    $scope.endGoToVector = function (vector) {
        if (currGoToAccount == vector.account) {
            $scope.address = $scope.io.lengthenAddress(vector.account); // set the search display
            $scope.goToVector(vector);
        }
        currGoToAccount = undefined;
    }
    $scope.goToVector = function (vector) {
        $scope.isAnimating = true;

            // Reset the selected node
            $scope.currFocalPoint.n = true;
            $scope.currFocalPoint.f = vector.account;

            var steps = 30;
            var center = frame.center();
            var dest_x = parseInt(vector.x, 10), dest_y = parseInt(vector.y, 10);
            var delta_x = center.x - dest_x, delta_y = center.y - dest_y;

            for (var i = 0; i < steps; i++) {
                var step = 2 / steps * (1 - Math.abs(1 - 2 * i / steps));
                setTimeout(function (params) {
                    frame.translate.x -= params.step_x;
                    frame.translate.y -= params.step_y;

                    // Required before the paint event
                    if (params.isLast) {
                        $scope.isAnimating = false;
                    }
                    paint(ctx, dag, frame, $scope.isAnimating, $scope.currFocalPoint, $scope.ui.darkTheme);

                    if (params.isLast) {
                        try {
                            $scope.nodes = dag.getAccountPositions(frame);
                            $scope.txs = dag.getTransactionPositions(frame);

                            // update the reference frame context & memory management
                            dag.clearObjectsOutsideContext(frame);
                            if (params.account) {
                                dagGraphics.resetBlocks(dag, frame);
                                $scope.io.searchSecondaryAddresses([params.account]);
                            }

                            $scope.$apply();
                        }
                        catch (Exception) { 

                        }
                    }
                }, 
                20 * i, { 
                    isLast: i == steps - 1, 
                    account: vector.account,
                    step_x: step * delta_x,
                    step_y: step * delta_y,
                });
            }
    }
    $scope.goToAccount = function (account) {
        // this needs to handle a generic account - check to see if dag.accounts contains it, if not, then reload page
        if (dag.accounts[account]) {
            // get location
            var p = frame.transformVector(dag.accounts[account].pos.location);
            $scope.goToVector({
                account: account,
                x: p.x + 'px',
                y: p.y + 'px'
            });
        }
        else {
            $scope.io.search(account);
        }
    }

    // Dragging functions ------------------------------------------------------------------
    var start, initialTranslate;
    function mousedown (event) {

        if (!ctx) return;

        event.preventDefault();
        console.log('mousedown');

        $scope.isAnimating = true;
        $scope.cursor = 'grabbing';
        $scope.$apply(); // apply is animating

        start = vector(event.screenX, event.screenY);   // the mouse position
        initialTranslate = copyVector(frame.translate); // the current translation position of the canvas

        $document.on('mousemove', mousedownMousemove);
        $document.on('mouseup', mouseup);
    }
    function mousedownMousemove(event) {

        if (!$scope.isAnimating) {
            $scope.isAnimating = true;   // not sure why this would even hit but it does
        }

        if (frame != undefined) {
            // set the new translate position
            frame.translate.x = initialTranslate.x - (event.screenX - start.x);
            frame.translate.y = initialTranslate.y - (event.screenY - start.y);
            paint(ctx, dag, frame, $scope.isAnimating, $scope.currFocalPoint, $scope.ui.darkTheme);
        }
    }
    function mouseup() {
        console.log('mouseup');

        // Stop listening to events first
        $document.unbind('mousemove', mousedownMousemove);
        $document.unbind('mouseup', mouseup);

        $scope.isAnimating = false;

        // requires 1 more paint event under isAnimating = false 
        paint(ctx, dag, frame, $scope.isAnimating, $scope.currFocalPoint, $scope.ui.darkTheme);
        if ($scope.intersectedHash) {
            paintMouseoverTransaction(ctx, dag, $scope.intersectedHash, frame, $scope.ui.darkTheme);
            $scope.cursor = "pointer";
        }

        try {
            $scope.nodes = dag.getAccountPositions(frame);
            $scope.txs = dag.getTransactionPositions(frame);
            $scope.cursor = 'grab';
            $scope.$apply();
        }
        catch (Exception) { }
        
        // update the reference frame context & memory management
        dag.clearObjectsOutsideContext(frame);
    }
    
    // Zoom functions -------------------------------------------------------------------
    function mousewheel (event) {

        if (!ctx) return;

        // cross-browser wheel delta
        var event = window.event || event; // old IE support
        var delta = Math.max(-1, Math.min(1, (event.wheelDelta || -event.detail)));

        // Prevent the entire page from scrolling up or down
        // for IE
        event.returnValue = false;
        // for Chrome and Firefox
        if (event.preventDefault) {
            event.preventDefault();
        }
        $scope.animateZoom(delta, vector(event.clientX, event.clientY));
    }

    var isZooming;
    $scope.animateZoom = function (delta, focus) {

        if (isZooming) {
            return;
        }
        $scope.isAnimating = true;
        $scope.$apply();

        isZooming = true;

        // Ease-in-out animation
        var maxScaleStep = (delta > 0 ? 0.05 : 0.025) * delta * frame.scale;
        var frames = 30;
        for (var i = 0; i < frames; i++) {

            setTimeout(function (params) {

                var scaleChange = (1 - Math.abs(1.0 - params.i / (frames / 2.0))) * maxScaleStep;

                var pointerX = (focus.x + frame.translate.x) / frame.scale;
                var pointerY = (focus.y + frame.translate.y) / frame.scale;

                frame.scale += scaleChange;
                frame.translate.x += scaleChange * pointerX;
                frame.translate.y += scaleChange * pointerY;
                
                // Need to set this prior to painting as there is no repaint event after this
                if (params.isLast) {
                    $scope.isAnimating = false;
                }

                paint(ctx, dag, frame, $scope.isAnimating, $scope.currFocalPoint, $scope.ui.darkTheme);

                if (params.isLast) {
                    isZooming = false;

                    try {
                        $scope.nodes = dag.getAccountPositions(frame);
                        $scope.txs = dag.getTransactionPositions(frame);
                        $scope.$apply();
                    }
                    catch (Exception) { }
                }
            }, 
            i * 20, { i: i, isLast: i == frames - 1});
        }
    }

    // Intersection detection --------------------------------------------------------------
    $scope.intersectedHash = undefined;
    function mousemove (event) {
        if (!dag || !dag.txs || !ctx) return;
        var txHashes = Object.keys(dag.txs);
        if (!txHashes) return;

        var newIntersectedHash = undefined;
        for (var t = 0; t < txHashes.length; t++) {

            // Ordered by the alphabetical order of tx hashes
            if (newIntersectedHash && newIntersectedHash > txHashes[t]) {
                continue;
            }

            var tx = dag.txs[txHashes[t]];
            if (detectIntersection(event, tx)) {
                newIntersectedHash = txHashes[t];
            }
        }

        if (newIntersectedHash != $scope.intersectedHash) {
            paint(ctx, dag, frame, $scope.isAnimating, $scope.currFocalPoint, $scope.ui.darkTheme);
            if (newIntersectedHash) {
                paintMouseoverTransaction(ctx, dag, newIntersectedHash, frame, $scope.ui.darkTheme);
                $scope.cursor = "pointer";
            }
            else {
                $scope.cursor = "grab";
            }
            $scope.$apply();
        }
        $scope.intersectedHash = newIntersectedHash;
    }

    function detectIntersection(p, tx) {
        var v1 = frame.transformVector(dag.accounts[tx.from].pos.location),
            v2 = frame.transformVector(dag.accounts[tx.to].pos.location);
        
        var width = frame.scale * frame.width,
                d = tx.graphics.length * width,
                h = tx.graphics.height * width;

        var hDist = ((p.x - v1.x) * (v2.x - v1.x) + (p.y - v1.y) * (v2.y - v1.y)) / (d * d);
        if (hDist <= 0.05 || hDist >= 0.95) return false;

        var vDist = (tx.graphics.curvature > 0 ? -1 : 1) * ((v2.y - v1.y) * p.x - (v2.x - v1.x) * p.y + v2.x * v1.y - v2.y * v1.x) / d;
        var expectedVDist = 4 * h * hDist * (1 - hDist);
        return Math.abs(expectedVDist - vDist) < 0.6 * tx.graphics.width;
    }

    $scope.onClick = function () {
        if ($scope.intersectedHash && $scope.txs) {
            for (var i = 0; i < $scope.txs.length; i++) {
                var tx = $scope.txs[i];
                if (tx.h == $scope.intersectedHash) {
                    showTransactionDetail(tx);
                }
            }
        }
    }
    function showTransactionDetail(tx) {
        tx.showDetail = true;
        tx.details = dag.txs[tx.h];

        if (!tx.fetchedDetails) {
            $http.post('/tx', { hash: tx.h }).then(
                function success (res) {
                    if (res.data.sendAcc) {
                        tx.details.from = res.data.sendAcc;
                    }
                    if (res.data.sendHash) {
                        tx.details.sendBlock = res.data.sendHash;
                    }
                    if (res.data.receiveAcc) {
                        tx.details.to = res.data.receiveAcc;
                    }
                    if (res.data.receiveHash) {
                        tx.details.receiveBlock = res.data.receiveHash;
                    }
                    if (res.data.sendType) {
                        tx.details.sendBlockType = res.data.sendType;
                    }
                    if (res.data.receiveType) {
                        tx.details.receiveBlockType = res.data.receiveType;
                    }
                    tx.fetchedDetails = true;
                    tx.aInt = getIntegerPart(tx.a);
                    tx.aDecimal = getDecimalPart(tx.a);
                },
                function error (err) {
                    console.log(err);
                    tx.details.sendBlock = tx.h;
                    tx.details.receiveBlock = tx.h;
                }
            )
        }
        
    }

    // Rounding of nano
    $scope.round = function (amount) {
        return amount.toFixed(2);
    }
    $scope.abbreviateAddress = function (address) {
        var key = address.split('_')[0];
        return 'nano_' + key.substring(0, 5) + '...' + key.substring(key.length - 5);
    }
    $scope.abbreviateHash = function (hash) {
        return hash.substring(0, 5) + '...' + hash.substring(hash.length - 5);
    }

    // Attach all listeners
    $document.on('mousedown', mousedown);
    $document.on('mousemove', mousemove);
    $document.bind("DOMMouseScroll mousewheel onmousewheel", mousewheel);

    // Remove all listeners
    $scope.$on('$destroy', function () {
        $document.unbind('mousedown', mousedown);
        $document.unbind('mousemove', mousemove);
        $document.unbind('DOMMouseScroll mousewheel onmousewheel', mousewheel);
    });

    // init with -------------------------------------------------------------------------
    $scope.ui.loading = true;
    $scope.ui.title = 'Nano Explorer';
    
    if ($routeParams["query"]) {
        $scope.io.submitAddress($routeParams["query"]);
    }
    else {
        $scope.io.submitAddress('nano_3t6k35gi95xu6tergt6p69ck76ogmitsa8mnijtpxm9fkcm736xtoncuohr3');
    }
});
function calculateAccountPositions(dag, dagGraphics, frame) {

    // sort by descending txs 
    var accounts = dag.getAccounts();
    accounts.sort(function (a, b) {
        return b.deg - a.deg;
    });

    // Find center and add source node
    if (dag.accounts[dag.rootAccount] && !dag.accounts[dag.rootAccount].set) {
        var center = vector((dagGraphics.xBlocks - 1) / 2, (dagGraphics.yBlocks - 1) / 2);
        dagGraphics.addNode(center, 1);
        dag.setLocation(dag.rootAccount, center, vector(0.5, 0.5 * frame.height / frame.width));
    }

    for (var i = 0; i < accounts.length; i++) {
        var a = accounts[i];
        if (a.account === dag.rootAccount) continue;

        var account = dag.accounts[a.account];
        if (!account.pos.set) {
            var sourceNodePosition = dag.accounts[account.pos.parent].pos.block_location;
            var block = dagGraphics.findLowestPenaltyBlock(sourceNodePosition);
            dagGraphics.addNode(block, calculateNodeWeight(a.deg));
            var location = dagGraphics.sampleNodeLocationWithinBlock(block, frame);
            dag.setLocation(a.account, block, location);
        }
    }
}
function calculateNodeWeight(degree) {
    return 1 + Math.log(1 + degree);
}
function calculateTransactionPositions(dag) {

    var now = new Date().getTime();
    var light = {
        r: 63, g: 115, b: 115
    };
    var dark = {
        r: 36, g: 66, b: 66
    }
    var nodeRadius = 10;

    var txHashes = Object.keys(dag.txs);
    if (txHashes != undefined) {
        for (var t = 0; t < txHashes.length; t++) {

            var tx = dag.txs[txHashes[t]];
            if (tx.graphics && tx.graphics.set) {
                continue;
            }

            // Calculate color
            var shadeRatio = Math.min(0.5, 1 - Math.exp((tx.d * 1000 - now) / 1e9));
            var lightColor = colorGradient(shadeRatio, light, {r: 255, g: 255, b: 255});
            var darkColor = colorGradient(0.5 - shadeRatio, dark, {r: 159, g: 195, b: 195});
            
            // Calculate the 'bezierControlPoint', controlling degree of curvature
            var fromAccount = dag.accounts[tx.from], toAccount = dag.accounts[tx.to];
            var curvature = 0.6 * Math.random() - 0.3;
            var tail = fromAccount.pos.location, head = toAccount.pos.location;
            var midpoint = middle(head, tail);
            var delta = subtractVector(head, tail);
            var bezierControlPoint = vector(midpoint.x - curvature * delta.y, midpoint.y + curvature * delta.x);
            var normal = subtractVector(bezierControlPoint, midpoint);
            var height = modulus(normal) / 2;

            var arrow_shaft_width = log2(Math.max(2, 100 * tx.a));
            var clip_head_norm = normalise(subtractVector(head, bezierControlPoint));
            var arrowhead_length = Math.max(arrow_shaft_width * 2, 10);
            var arrow_shaft_orth = orthogonalVector(clip_head_norm, arrowhead_length * 0.75);
            var arrowhead_shaft = multiply(clip_head_norm, -arrowhead_length);
            
            tx.graphics = {
                set: true,
                bcp: bezierControlPoint,
                clip_head: multiply(clip_head_norm, arrow_shaft_width * 1.8),                               // the vector that we clip off the head
                clip_tail: multiply(normalise(subtractVector(tail, bezierControlPoint)), nodeRadius * 1.5), // the vector that we clip off the tail

                length: modulus(delta),
                height: height,
                curvature: curvature,
                width: arrow_shaft_width,
                arrow_size: Math.max(arrow_shaft_width * 2, 10),
                lightColor: rgbToHex(lightColor),
                darkColor: rgbToHex(darkColor),
                lightBorderColor: rgbToHex(incrementColor(lightColor, -20)),
                darkBorderColor: rgbToHex(incrementColor(darkColor, 15)),
                head_to_arrow_tip: multiply(clip_head_norm, -nodeRadius),             // <tip> - <head>
                arrow_tip_left: addVector(arrowhead_shaft, arrow_shaft_orth),           // position rel. arrow head
                arrow_tip_right: subtractVector(arrowhead_shaft, arrow_shaft_orth),     // position rel. arrow head

                label_location: middle(midpoint, bezierControlPoint),
                label_rotation: Math.atan(delta.y / delta.x)
            };
        }
    }
}
function shiftToFocus(dag, frame) {
    
    // By default, the positions are calculated relative to the sender node, 
    // and so the center node will always be positioned in the middle of the screen at this point.
    // If the focus point is instead a transaction, we want to translate the canvas so that 
    // the middle of the transaction is instead in the middle 
    if (dag.focalPoint && !dag.focalPoint.n) {
        var hash = dag.focalPoint.f;
        if (!dag.txs[hash]) {
            console.log('cannot find tx');
            return;
        }
        var curr_location = frame.transformVector(dag.txs[hash].graphics.label_location);
        var desired_location = frame.center();
        frame.translate.x -= (desired_location.x - curr_location.x);
        frame.translate.y -= (desired_location.y - curr_location.y);
    }
}
function entryAnimationFrame(op) {

    var duration = 700;
    var progress = (op.timestamp - op.startTime) / duration;
    progress = clamp(progress, 0, 1);

    var accs = Object.keys(op.dag.accounts);
    var txs = Object.keys(op.dag.txs);

    if (op.zoom) {
        var scaleChange = (op.frame.defaultScale - 1) * easeOut(progress);
        var newScale = op.originalScale + scaleChange;
        op.frame.scale = newScale;
        op.frame.translate.x = newScale * op.originalTranslate.x + scaleChange * op.ctx.canvas.width / 2;
        op.frame.translate.y = newScale * op.originalTranslate.y + scaleChange * op.ctx.canvas.height / 2;
    }

    var i;
    for (i = 0; i < accs.length; i++) {
        var a = op.dag.accounts[accs[i]].pos;
        // only target unfinished accounts
        if (a.opening_state <= 0.999) {
            a.opening_state = progress;
        }
    }
    for (i = 0; i < txs.length; i++) {
        var tx = op.dag.txs[txs[i]];
        // only target unfinished transactions
        if (tx.opening_state <= 0.999) {
            tx.opening_state = progress;
        }
    }

    if (progress <= 0.999) {
        paint(op.ctx, op.dag, op.frame, true, op.focalPoint, op.darkTheme);
        window.requestAnimationFrame(function (timestamp) {
            op.timestamp = timestamp;
            entryAnimationFrame(op);
        });
    }

    // The final animation frame
    else {
        paint(op.ctx, op.dag, op.frame, /*!op.isLastSequence*/ false, op.focalPoint, op.darkTheme);
        op.callback(op.dag);
        return;
    }
}
function paint(ctx, dag, frame, isAnimating, focalPoint, isDark) {
    ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);

    // Draw the txs
    var txHashes = Object.keys(dag.txs);
    if (txHashes != undefined) {

        var textColor = isDark ? '#091010' : "#325b5b";
        for (var t = 0; t < txHashes.length; t++) {
            var tx = dag.txs[txHashes[t]];
            var tail = frame.transformVector(dag.accounts[tx.from].pos.location),
                head = frame.transformVector(dag.accounts[tx.to].pos.location),
                bcp = frame.transformVector(tx.graphics.bcp);
            
            if (tx.graphics) {

                var c = isDark ? tx.graphics.darkColor : tx.graphics.lightColor;
                var b = isDark ? tx.graphics.darkBorderColor : tx.graphics.lightBorderColor;
                addCurve(ctx, head, tail, bcp, tx.graphics, tx.opening_state, c, b);

                var drawLabel = tx.a > frame.minTxSizeForLabel || (!focalPoint.n && txHashes[t] == focalPoint.f);
                if (!isAnimating && drawLabel) {
                    var textLocation = frame.transformVector(tx.graphics.label_location);
                    addText(ctx, tx.a.toFixed(0), textLocation, tx.graphics, textColor);
                }
            }
        }
    }

    // Draw the nodes
    var nodeBorderColor = isDark ? '#3f7373' : '#000000';
    var nodeFillColor = isDark ? '#1b3131' : '#333333';

    var accounts = Object.keys(dag.accounts);
    for (var i = 0; i < accounts.length; i++) {
        var pos = dag.accounts[accounts[i]].pos;
        var location = frame.transformVector(pos.location);
        addCircle(ctx, location, nodeRadius, nodeBorderColor, nodeFillColor, pos.opening_state, isDark);

        if (focalPoint.n && accounts[i] == focalPoint.f) {
            var h = 5 * nodeRadius;
            drawMapSymbol(ctx, location.x - h / 2, location.y - h - 0.6 * nodeRadius, h, isDark);
        }
    }

    // Draw the map symbol for tx at the end, so it doesn't get overlapped
    if (!focalPoint.n && focalPoint.f && txHashes != undefined) {
        for (var t = 0; t < txHashes.length; t++) {

            // Need this inside paint() because we're drawing the map symbol at every step
            if (txHashes[t] == focalPoint.f) {
                var tx = dag.txs[txHashes[t]];
                var h = 5 * nodeRadius;
                var p = frame.transformVector(tx.graphics.label_location);
                drawMapSymbol(ctx, p.x - h / 2, p.y - h, h, isDark);
            }
        }
    }
}
function drawMapSymbol(ctx, x, y, dim, isDark) {
    var el = document.getElementById("map_symbol");
    if (!el) return;

    ctx.drawImage(el, isDark ? 110 : 55, 55, 50, 50, x, y, dim, dim);
}
function addCircle(ctx, center, radius, borderColor, fillColor, completion) {

    ctx.beginPath();
    ctx.lineWidth = 2;
    ctx.arc(center.x, center.y, radius, 0, 2 * Math.PI);

    ctx.strokeStyle = borderColor;
    ctx.stroke();

    ctx.fillStyle = fillColor;
    ctx.fill();
}
function addCurve(ctx, head, tail, c, graphics, lambda, color, borderColor) {

    // is completion != 1
    if (lambda <= 0.999) {
        // a -> c -> b  
        var mid_pt = middle(head, tail);
        var base = subtractVector(head, tail);
        var orth = subtractVector(c, mid_pt);

        var y_prop = 2 * lambda * (1 - lambda);
        head = {
            x: tail.x + base.x * lambda + orth.x * y_prop,
            y: tail.y + base.y * lambda + orth.y * y_prop
        };
        c = {
            x: tail.x + 0.5 * base.x * lambda + orth.x * lambda,
            y: tail.y + 0.5 * base.y * lambda + orth.y * lambda
        };
    }

    /* Clip indicates the length off each end that we chop off, in the direction of c - x */
    var arrow_tail = subtractVector(tail, graphics.clip_tail);    // clip the tail of the arrow
    var arrow_head = subtractVector(head, graphics.clip_head);  // clip the head of the arrow
    var arrow_tip = addVector(head, graphics.head_to_arrow_tip);
    var arrow_left = addVector(arrow_tip, graphics.arrow_tip_left);
    var arrow_right = addVector(arrow_tip, graphics.arrow_tip_right);

    // Border
    ctx.beginPath();
    ctx.lineWidth = graphics.width + 3;
    ctx.moveTo(arrow_tail.x, arrow_tail.y);
    ctx.quadraticCurveTo(c.x, c.y, arrow_head.x, arrow_head.y);
    ctx.strokeStyle = borderColor;
    ctx.stroke();

    var border_x = multiply(normalise(graphics.clip_head), 2);
    var border_y = multiply(normalise(subtractVector(arrow_left, arrow_right)), 3);
    addTriangle(ctx, addVector(arrow_tip, border_x), 
                    subtractVector(addVector(arrow_left, border_y), border_x),
                    subtractVector(subtractVector(arrow_right, border_y), border_x),
                    borderColor);

    // Fill
    ctx.beginPath();
    ctx.lineWidth = graphics.width;
    ctx.moveTo(arrow_tail.x, arrow_tail.y);
    ctx.quadraticCurveTo(c.x, c.y, arrow_head.x, arrow_head.y);
    ctx.strokeStyle = color;
    ctx.stroke();

    addTriangle(ctx, arrow_tip, arrow_left, arrow_right, color);
}
function addText(ctx, text, location, graphics, color) {

    ctx.font = "bold " +  graphics.width + "px Arial";
    ctx.fillStyle = color;
    ctx.textAlign = "center";
    ctx.save();
    ctx.translate(location.x, location.y);
    ctx.rotate(graphics.label_rotation);
    ctx.fillText(text, -text.length / 2, graphics.width / 2);
    ctx.restore();
}
function addTriangle(ctx, v1, v2, v3, fillColor) {

    ctx.beginPath();
    ctx.moveTo(v1.x, v1.y);
    ctx.lineTo(v2.x, v2.y);
    ctx.lineTo(v3.x, v3.y);

    ctx.fillStyle = fillColor;
    ctx.fill();
}
function paintMouseoverTransaction(ctx, dag, txHash, frame, isDark) {
    if (!ctx || !dag || !frame || !txHash) return;

    var tx = dag.txs[txHash];
    var tail = frame.transformVector(dag.accounts[tx.from].pos.location),
        head = frame.transformVector(dag.accounts[tx.to].pos.location),
        bcp = frame.transformVector(tx.graphics.bcp);

    if (!tx.graphics) return;

    var color = isDark ? '#FFFFFF' : '#003333';
    var borderColor = isDark ? '#FFFFFF' : '#003333';
    addCurve(ctx, head, tail, bcp, tx.graphics, tx.opening_state, color, borderColor);
    var textLocation = frame.transformVector(tx.graphics.label_location);
    addText(ctx, tx.a.toFixed(0), textLocation, tx.graphics, isDark ? '#003333' : '#FFFFFF');
}
function colorGradient(x, startColor, endColor) {
    return {
        r: startColor.r + (endColor.r - startColor.r) * x, 
        g: startColor.g + (endColor.g - startColor.g) * x, 
        b: startColor.b + (endColor.b - startColor.b) * x
    };
}
function incrementColor(color, amount) {
    return {
        r: clamp(color.r + amount, 0, 255),
        g: clamp(color.g + amount, 0, 255),
        b: clamp(color.b + amount, 0, 255)
    };
}