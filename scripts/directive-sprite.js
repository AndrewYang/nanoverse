app.directive('nnSprite', function() {
    return {
        restrict: 'E',
        template: "<div ng-style=\"{'background-size': w + ' ' + h, 'background-position': x + ' ' + y, 'padding-bottom': height, width: width }\"></div>",
        scope: {
            ui: '=',
            name: '=',
            width: '='
        },
        link: function(scope) {

            var rect = scope.ui.sprites[scope.name];
            var size = scope.ui.sprites['size'];

            scope.w = 100 * size.w / rect.w + '%';
            scope.h = 100 * size.h / rect.h + '%';
            scope.x = 100 * rect.x / (size.w - rect.w) + '%';
            scope.y = 100 * rect.y / (size.h - rect.h) + '%';

            scope.height = (100 * rect.h / rect.w) + '%';
        }
    }
});