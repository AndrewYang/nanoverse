var app = angular
.module("app", [ "ngRoute" ])
.config(function ($routeProvider, $locationProvider) {

    $locationProvider.html5Mode(true);

    $routeProvider
        .when("/", {
            templateUrl: "graph.html",
            controller: "graphCtrl"
        })
        .when("/map/:query", {
            templateUrl: "graph.html",
            controller: "graphCtrl"
        })
        .when("/account/:accountId", {
            templateUrl: "../account.html",
            controller: "accountCtrl"
        })
        .when("/block/:blockId", {
            templateUrl: "../block.html",
            controller: "blockCtrl"
        })
        .otherwise({
            redirectTo: '/'
        })
})
.controller("ctrl", function ($scope, $http, $window, $location) {
    $scope.dag = {};
    $scope.ui = {
        size: getWindowSize(),
        isMobileDevice: mobileCheck(),
        loading: true,
        loadingBarColor: '#325b5b',
        addressPasted: false,
        sprites: {
            size: { w: 270, h: 110 },
            camera: { x: 0, y: 0, w: 50, h: 50 },
            moon: { x: 55, y: 0, w: 50, h: 50 },
            logo: { x: 110, y: 0, w: 50, h: 50 },
            sun: { x: 0, y: 55, w: 50, h: 50 },
            map: { x: 55, y: 55, w: 50, h: 50 },
            map_light: { x: 110, y: 55, w: 50, h: 50 },
            paste: { x: 165, y: 0, w: 50, h: 50 },
            paste_light: { x: 165, y: 55, w: 50, h: 50 },
            search: { x: 220, y: 0, w: 50, h: 50 },
            menu: { x: 220, y: 55, w: 50, h: 50 }
        },
        ignoreAddressChangeOnce: true,
        cutoffDate: 1560837000
    };
    $scope.static = {
        default_addresses: [
            {
                alias: 'Genesis',
                address: 'nano_3t6k35gi95xu6tergt6p69ck76ogmitsa8mnijtpxm9fkcm736xtoncuohr3'
            },
            {
                alias: 'Developer Fund',
                address: 'nano_1ipx847tk8o46pwxt5qjdbncjqcbwcc1rrmqnkztrfjy5k7z4imsrata9est'
            },
            {
                alias: 'NanoFaucet',
                address: 'nano_3x7cjioqahgs5ppheys6prpqtb4rdknked83chf97bot1unrbdkaux37t31b'
            },
            {
                alias: 'Binance Rep',
                address: 'nano_3jwrszth46rk1mu7rmb4rhm54us8yg1gw3ipodftqtikf5yqdyr7471nsg1k'
            },
            {
                alias: 'BrainBlocks Rep',
                address: 'nano_1brainb3zz81wmhxndsbrjb94hx3fhr1fyydmg6iresyk76f3k7y7jiazoji'
            }
        ],
        nano_url: 'https://nano.org',
        donate: 'nano_11az7pd714wccmicu78jjwwiebo3zxqc8pw3xoktyxcyoadaxnqqek6f61p4'
    };
    $scope.aliases = {};
    $scope.query = {
        type: 'send',
        minAmount: undefined,
        maxAmount: undefined,
        numTx: 100,
        minDate: undefined,
        maxDate: undefined,
        includeUnkDates: true
    };

    $scope.io = {

        shortenAddress: function (s) {
            var address = undefined;
            if (!s) return address;
        
            s = s.toLowerCase().trim();
            if (s.substring(0, 5) == 'nano_') {
                address = s.substring(5);
            }
            else if (s.substring(0, 4) == 'xrb_') {
                address = s.substring(4);
            }
        
            return address;
        },
        lengthenAddress: function (k) {
            if (!k || k.length == 0) return '';
            return 'nano_' + k;
        },

        submitAddress: function (query) {

            // Argument-supplied addresses take precedence
            if (!query) {
                query = $scope.address;
            }
            
            var path = $location.path();
            if (path != '/' && path.indexOf('/map/') != 0) {
                $location.path('/map/' + query);
            }

            $http.post('/search', { q: query, filters: $scope.query }).then(
                function success (res) {
                    var data = decode(res.data);

                    console.log('search result:');
                    console.log(data);

                    updateAliases(data.aliases);
                    
                    $scope.dag = new Dag(data);
                    $scope.ui.success = true;

                    if (data.focalPoint) {
                        // Focal point is an account
                        if (data.focalPoint.n) {
                            $scope.address = $scope.io.lengthenAddress(data.focalPoint.f);
                        }
                        else {
                            $scope.address = data.focalPoint.f;
                        }
                    }
                    
                    $scope.$broadcast('dagInit', $scope.dag);
                },
                function error (err) {

                    console.log(err.data);

                    $scope.ui.loading = false;
                    $scope.ui.success = false;
                    $location.url(err.data.redirect);
                }
            )
        },
        searchSecondaryAddresses: function (addresses) {
    
            if (addresses.length == 0) {
                return;
            }
            
            // This is the list of potential address - validated client side first
            var a = addresses.splice(0, 30);
            if (!a || a.length == 0) return;

            // Validated addresses
            var array = [];
            for (var i = 0; i < a.length; i++) {
                if (isValidAddressKey(a[i])) {
                    array.push(a[i]);
                }
            }

            // post
            $http.post('/searches', { addresses: array, filters: $scope.query }).then(
                function success (res) {

                    var data = decode(res.data);

                    console.log('secondary search result:');
                    console.log(data);

                    updateAliases(data.aliases);

                    // Note we don't initialise a new Dag object
                    // This will mean we don't change the isComplete, focalPoint etc.
                    $scope.dag.add(data);
                    $scope.$broadcast('dagUpdate', $scope.dag);
                    $scope.io.searchSecondaryAddresses(addresses);
                },
                function error (err) {
                    console.log(err);
                    $scope.io.searchSecondaryAddresses(addresses);
                }
            )
        },
        
        // Convenience method
        search: function (address) {
            $scope.address = address;
            $scope.io.submitAddress();
        }
    };
    
    function updateAliases (aliases) {
        if (!aliases) {
            return;
        }
        for (var i = 0; i < aliases.length; i++) {
            $scope.aliases[aliases[i].account] = aliases[i].alias;
        }
    }

    $scope.changeTheme = function () {
        $scope.ui.darkTheme = !$scope.ui.darkTheme;
        $scope.ui.loadingBarColor = $scope.ui.darkTheme ? '#9cc9c9' : '#325b5b';
    }
    $scope.screenshot = function () {
        var link = document.createElement('a');
        link.href = document.getElementById('chart-graph').toDataURL('png');
        link.download = 'Screenshot.png';

        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    }
    $scope.copyAccount = function (value) {
        if ($scope.copyUsingNav(value)) {
            $scope.ui.addressPasted = true;
        }
    }
    // copy by creating a new text area
    $scope.copy = function (value) {
        var temp = document.createElement("textarea");
        temp.value = value;
        document.body.appendChild(temp);
        temp.focus();
        temp.select();
        var success = document.execCommand('copy');
        document.body.removeChild(temp);
        return success;
    }
    $scope.copyUsingNav = function (text) {
        if (!navigator.clipboard) {
            return false;
        }
        navigator.clipboard.writeText(text);
        return true;
    }

    $scope.$watch('address', function (newVal, oldVal) { 
        if (newVal != oldVal) {
            $scope.ui.addressPasted = false; 

            // We don't want to change on the initial address
            if ($scope.ui.ignoreAddressChangeOnce) {
                $scope.ui.ignoreAddressChangeOnce = false;
                return;
            }

            var alias = $scope.aliases[$scope.io.shortenAddress(newVal)];
            if (alias) {
                $scope.gui_address = alias;
            }
            else {
                $scope.gui_address = newVal;
            }
        }
    });
    $scope.formatDate = function (time) {
        if (time < 1) {
            return 'Unknown timestamp';
        }
        return new Date(time).toLocaleString('default', { year: 'numeric', month: 'short', day: '2-digit', hour12: true, hour: 'numeric', minute: 'numeric' });
    }

    $scope.recalculateDisplay = function () {
        $scope.ui.size = getWindowSize();
        $scope.ui.isMobileDisplay = $scope.ui.size.width < 780 || $scope.ui.isMobileDevice;
        console.log('recalculating display: ' + $scope.ui.isMobileDisplay + '\t' + JSON.stringify($scope.ui.size));
    }

    // watch resize event 
    angular.element($window).bind('resize', function () {
        $scope.recalculateDisplay();
        $scope.$apply();
    });

    // Start
    $scope.recalculateDisplay();
});

function getWindowSize() {
    var body = document.body, html = document.documentElement;
    return {
        height: window.innerHeight || html.clientHeight || body.clientHeight,
        width: window.innerWidth || html.clientWidth || body.clientWidth
    }
}
function isValidAddressKey(address) {
    if (!address || address.length != 60) return;

    // Test for alphanumeric chars only - the address should already be in lower case
    return /^[a-z0-9]+$/i.test(address);
}

// Methods for decoding from uint8 array
function decode (json) {
    // Decode dag structure from re-inflated, condensed uint8[]
    var accounts = decodeAccounts(json.a);
    var aliases = decodeAliases(accounts, json.l, json.n);
    var txs = decodeTransactions(json.t);
    return {
        accounts: accounts,
        aliases: aliases,
        parents: json.p,
        txs: txs,
        complete: json.c,
        focalPoint: json.f
    }
}
function decodeAccounts (s) {

    // Fail silently
    if (!s || s.length % 60 != 0) {
        return [];
    }

    var n = s.length / 60;
    var accounts = [];
    for (var i = 0; i < n; i++) accounts.push(s.substring(i * 60, (i + 1) * 60));
    return accounts;
}
function decodeAliases (accounts, indexes, names) {

    // Fail silently
    if (!indexes || !names || !accounts || indexes.length != names.length) {
        return [];
    }

    var aliases = [];
    for (var i = 0; i < indexes.length; i++) {
        if (indexes[i] < accounts.length) {
            aliases.push({
                account: accounts[indexes[i]],
                alias: names[i]
            });
        }
    }
    return aliases;
}
function decodeTransactions (s) {

    // Fail silently
    if (!s) {
        return [];
    }

    var array = s.split(',');
    var txs = [];
    for (var i = 0; i < array.length; i += 6) {
        txs.push({
            s: parseInt(array[i]),
            f: parseInt(array[i + 1]),
            t: parseInt(array[i + 2]),
            h: array[i + 3],
            a: parseFloat(array[i + 4]),
            d: parseInt(array[i + 5])
        });
    }
    return txs;
}
function mobileCheck() {
    //return true;
    var check = false;
    (function (a) { if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true; })(navigator.userAgent || navigator.vendor || window.opera);
    return check;
};

