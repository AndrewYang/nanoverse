app.controller("blockCtrl", function ($scope, $routeParams, $http) {

    $scope.blockResponseReceived = false;

    function getBlockDetails (block) {
        if (!block) return;

        $http.post('/blockdetail', { block: block }).then(
            function success (res) {
                console.log(res);

                var b = res.data;

                if (b.aliases) {
                    if (b.account && b.aliases[b.account]) b.accountAlias = b.aliases[b.account];
                    if (b.rep && b.aliases[b.rep]) b.repAlias = b.aliases[b.rep];
                    if (b.prevRep && b.aliases[b.prevRep]) b.prevRepAlias = b.aliases[b.prevRep];
                    if (b.sender && b.aliases[b.sender]) b.senderAlias = b.aliases[b.sender];
                    if (b.recipient && b.aliases[b.recipient]) b.recipientAlias = b.aliases[b.recipient];
                }

                if (b.account)  b.account = $scope.io.lengthenAddress(b.account);
                if (b.rep)      b.rep = $scope.io.lengthenAddress(b.rep);
                if (b.prevRep)  b.previousRep = $scope.io.lengthenAddress(b.prevRep);
                if (b.sender)   b.sender = $scope.io.lengthenAddress(b.sender);
                if (b.recipient) b.recipient = $scope.io.lengthenAddress(b.recipient);

                if (!b.amount) b.amount = 0;
                b.amountInt = getIntegerPart(b.amount);
                b.amountDecimal = getDecimalPart(b.amount);

                if (!b.balance) b.balance = 0;
                b.balanceInt = getIntegerPart(b.balance);
                b.balanceDecimal = getDecimalPart(b.balance);

                // break up sig -> sig1, sig2
                if (b.sig) {
                    b.sig1 = b.sig.substring(0, b.sig.length / 2);
                    b.sig2 = b.sig.substring(b.sig.length / 2);
                }

                if (b.action) {
                    b.action = capitalise(b.action);
                }

                if (b.type && b.type.length > 0) {
                    b.type = capitalise(b.type);
                }

                b.timestamp = $scope.formatDate(b.timestamp * 1000);

                $scope.blockDetails = b;
                $scope.blockResponseReceived = true;
            },
            function error (err) {
                console.log(err);
                $scope.blockResponseReceived = true;
                $scope.searchBlock = block;
            }
        )
    }

    var block = $routeParams["blockId"];
    $scope.ui.title = 'Block - ' + block;
    if (block) {
        getBlockDetails(block);
    }
});