app.directive('nnCopyableField', function() {
    return {
        restrict: 'E',
        templateUrl: "copyableField.html",
        scope: {
            ui: '=',
            alias: '=',
            type: '=',
            field: '='
        },
        link: function(scope, el, at) {

            scope.explorerLink = '/' + scope.type + '/' + scope.field;
            scope.mapLink = '/map/' + scope.field;
            scope.copied = false;

            scope.copy = function (text) {
                if (!navigator.clipboard) {
                    return false;
                }
                navigator.clipboard.writeText(text);
                scope.copied = true;
                return true;
            }
        }
    }
});