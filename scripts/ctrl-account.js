
app.controller("accountCtrl", function ($scope, $routeParams, $http) {


    // Localise this function so we are not polluting the scope
    function getAccountDetails (account) {
        $http.post('/accountdetail', { account: account }).then(
            function success (res) {
                console.log(res);

                var a = res.data;
                if (a.alias) {
                    if (a.alias[a.account]) {
                        a.accountAlias = a.alias[a.account];
                    }
                    else {
                        // For benefit of future accounts on the same page
                        a.alias[a.account] = 'This account';
                    }
                    if (a.alias[a.rep]) a.repAlias = a.alias[a.rep];
                }

                a.rep = $scope.io.lengthenAddress(a.rep);

                if (!a.balance) a.balance = 0;
                a.balanceInt = getIntegerPart(a.balance);
                a.balanceDecimal = getDecimalPart(a.balance);

                if (!a.pending) a.pending = 0;
                a.pendingInt = getIntegerPart(a.pending);
                a.pendingDecimal = getDecimalPart(a.pending);

                if (!a.wt) a.wt = 0;
                a.wtInt = getIntegerPart(a.wt);
                a.wtDecimal = getDecimalPart(a.wt);

                if (a.transactions) {
                    for (var i = 0; i < a.transactions.length; i++) {
                        completeTransaction(a.transactions[i], a.alias);
                    }
                }
                if (a.pt) {
                    for (var i = 0; i < a.pt.length; i++) {
                        completeTransaction(a.pt[i], a.alias);
                    }
                }

                $scope.ad = a;
            },
            function error (err) {
                $scope.ad = {
                    error: true
                };
            }
        );
    }
    function completeTransaction (tx, alias) {
        if (alias[tx.a]) tx.alias = alias[tx.a];
        if (alias[tx.r]) tx.repAlias = alias[tx.r];

        tx.a = $scope.io.lengthenAddress(tx.a);
        tx.r = $scope.io.lengthenAddress(tx.r);
        
        if (tx.y == 'send') {
            tx.accType = 'to';
        } else if (tx.y == 'receive') {
            tx.accType = 'from';
        }

        // replace type with subtype if available
        if (tx.s) tx.y = tx.s;
        tx.y = capitalise(tx.y);

        tx.amountInt = getIntegerPart(tx.n);
        tx.amountDecimal = getDecimalPart(tx.n);
    }

    $scope.loadMoreTransactions = function (account, head) {

        if (!account || !head) {
            return;
        }
        $scope.ad.loadMoreTxError = false;

        $http.post('/extendhistory', { account: account, head: head }).then(
            function success (res) {
                var a = res.data;
                for (var i = 0; i < a.transactions.length; i++) {
                    completeTransaction(a.transactions[i], a.alias);
                    $scope.ad.transactions.push(a.transactions[i]);
                }
                $scope.ad.next = a.next;
            },
            function error (err) {
                $scope.ad.loadMoreTxError = true;
            }
        )
    }
    
    $scope.accountId = $routeParams["accountId"];
    $scope.ui.title = 'Account - ' + $scope.accountId;
    if ($scope.accountId) {

        var parts = $scope.accountId.split('_');
        $scope.shortAccountId = parts[0] + '_' + parts[1].substring(0, 4) + '...' + parts[1].substring(parts[1].length - 4);
        getAccountDetails($scope.accountId);
    }
});

function capitalise (s) {
    if (!s || s.length == 0) return ;
    return s.substring(0, 1).toUpperCase() + s.substring(1);
}
function getIntegerPart (n) {
    var s = n.toFixed(6);
    var i = parseInt(s.substring(0, s.length - 7));
    return i.toLocaleString(undefined,
        { minimumFractionDigits: 0, maximumFractionDigits: 0 });
}
function getDecimalPart (n) {
    var s = (n % 1).toFixed(6);
    if (s.length > 1) {
        return s.substring(1);
    }
    return '';
}